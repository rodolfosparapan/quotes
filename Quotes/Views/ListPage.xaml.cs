﻿using System;
using System.Collections.Generic;
using Quotes.Models;
using Quotes.ViewModels;
using Xamarin.Forms;

namespace Quotes.Views
{
    public partial class ListPage : ContentPage
    {
        public ListViewModel ViewModel { get; set; }

        public ListPage()
        {
            InitializeComponent();
            this.ViewModel = new ListViewModel();
            this.BindingContext = this.ViewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.LoadQuotes();

            MessagingCenter.Subscribe<ListViewModel, string>(this, "DeleteMessage", (ListViewModel, msg) => {
                DisplayAlert("Delete Message", msg, "OK");
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<ListViewModel, string>(this, "DeleteMessage");
        }

        private async void LoadQuotes()
        {
            await this.ViewModel.GetQuotes();
        }
    }
}
