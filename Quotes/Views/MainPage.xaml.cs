﻿using System;
using System.Collections.Generic;
using Quotes.Models;
using Quotes.ViewModels;
using Xamarin.Forms;

namespace Quotes.Views
{
    public partial class MainPage : TabbedPage
    {
        private NavigationPage listPageNavigation;

        public MainPage()
        {
            InitializeComponent();

            this.listPageNavigation = new NavigationPage(new ListPage());
            listPageNavigation.Title = "List Quotes";
            if (Device.RuntimePlatform == Device.iOS)
                listPageNavigation.Icon = "icon-list";
            Children.Add(listPageNavigation);

            var addPage = new NavigationPage(new AddPage());
            addPage.Title = "Add Quote";
            if (Device.RuntimePlatform == Device.iOS)
                addPage.Icon = "icon-add";
            Children.Add(addPage);

            var aboutPage = new NavigationPage(new AboutPage());
            aboutPage.Title = "About Page";
            if (Device.RuntimePlatform == Device.iOS)
                aboutPage.Icon = "icon-about";
            Children.Add(aboutPage);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<BaseViewModel, int>(this, "SetTabbledPageFocus", (addViewModel, tabbleItem) =>
            {
                CurrentPage = Children[tabbleItem];
            });

            MessagingCenter.Subscribe<BaseViewModel, Quote>(this, "PushEditPage", (addViewModel, quote) =>
            {
                listPageNavigation.Navigation.PushAsync(new AddPage(quote));
            });

            MessagingCenter.Subscribe<BaseViewModel>(this, "PopEditPage", (addViewModel) =>
            {
                listPageNavigation.Navigation.PopAsync();
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<BaseViewModel, int>(this, "SetTabbledPageFocus");
            MessagingCenter.Unsubscribe<BaseViewModel, Quote>(this, "PushEditPage");
            MessagingCenter.Unsubscribe<BaseViewModel>(this, "PopEditPage");
        }
    }
}
