﻿using System;
using System.Collections.Generic;
using Quotes.Models;
using Quotes.ViewModels;
using Xamarin.Forms;

namespace Quotes.Views
{
    public partial class AddPage : ContentPage
    {
        public AddPage(Quote quote = null)
        {
            InitializeComponent();
            BindingContext = new AddViewModel(quote);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<AddViewModel, string>(this, "AddMessage", (addViewModel, msg) =>
            {
                DisplayAlert("Message", msg, "OK");
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<AddViewModel, string>(this, "AddMessage");
        }
    }
}
