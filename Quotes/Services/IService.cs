﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Quotes.Services
{
    public interface IService<T>
    {
        Task<bool> AddAsync(T item);
        Task<bool> DeleteAsync(T item);
        Task<bool> UpdateAsync(T item);
        Task<List<T>> ListAsync();
    }
}
