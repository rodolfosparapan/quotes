﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quotes.Models;

namespace Quotes.Services
{
    public class QuotesMockData : IService<Quote>
    {
        private static List<Quote> quotes;
        private static bool alreadyLoaded = false;
        private static int incrementID = 6;

        public QuotesMockData()
        {
            if (!alreadyLoaded)
            { 
                QuotesMockData.quotes = new List<Quote>(){
                    new Quote(){Id=1, Text = "As Rosas são vermelhas como o mar", Author="Rodolfo"},
                    new Quote(){Id=2, Text = "Nem tudo que brilha é brilhante", Author="José Silva"},
                    new Quote(){Id=3, Text = "Dois pratos de tigro para dois tigres", Author="Adriana Caetano"},
                    new Quote(){Id=4, Text = "Aff, que coisa chata", Author="Brunno Adagio"},
                    new Quote(){Id=5, Text = "É nóis que voa bixão", Author="Pedro Paulo"},
                };
                alreadyLoaded = true;
            }
        }

        public async Task<bool> AddAsync(Quote item)
        {
            item.Id = QuotesMockData.incrementID;
            QuotesMockData.quotes.Add(item);
            QuotesMockData.incrementID++;
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteAsync(Quote item)
        {
            QuotesMockData.quotes.Remove(item);
            return await Task.FromResult(true);
        }

        public async Task<List<Quote>> ListAsync()
        {
            return await Task.FromResult(QuotesMockData.quotes);
        }

        public async Task<bool> UpdateAsync(Quote item)
        {
            var _item = QuotesMockData.quotes.Where((Quote arg) => arg.Id == item.Id).FirstOrDefault();
            QuotesMockData.quotes.Remove(_item);
            QuotesMockData.quotes.Add(item);

            return await Task.FromResult(true);
        }
    }
}
