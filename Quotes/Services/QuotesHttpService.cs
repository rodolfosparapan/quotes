﻿using System;
using Quotes.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Quotes.Services
{
    public class QuotesHttpService : IService<Quote>
    {
        private HttpClient httpClient;
        private const string serverUri = "http://localhost/citacoesapi/";

        public QuotesHttpService()
        {
            httpClient = new HttpClient();
        }

        public async Task<bool> AddAsync(Quote quote)
        {
            var resposta = await this.httpClient.PostAsync(serverUri + "add", this.GetQuoteJSON(quote));
            return resposta.IsSuccessStatusCode;
        }

        public async Task<List<Quote>> ListAsync()
        {
            var serverResult = await this.httpClient.GetStringAsync(serverUri + "get");
            var citacoesJson = JsonConvert.DeserializeObject<Citacao[]>(serverResult);

            var quotes = new List<Quote>();
            foreach (Citacao citacao in citacoesJson)
            {
                quotes.Add(new Quote() { Text = citacao.citacao, Author = citacao.autor });
            }

            return quotes;
        }

        public async Task<bool> DeleteAsync(Quote quote)
        {
            var resposta = await this.httpClient.DeleteAsync(serverUri + "del?id_citacao=" + quote.Id);
            return resposta.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateAsync(Quote quote)
        {
            var resposta = await this.httpClient.PutAsync(serverUri + "edit", this.GetQuoteJSON(quote));
            return resposta.IsSuccessStatusCode;
        }

        private StringContent GetQuoteJSON(Quote quote)
        {
            var json = JsonConvert.SerializeObject(new
            {
                citacao = quote.Text,
                autor = quote.Author
            });

            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}
