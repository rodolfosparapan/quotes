﻿using System;
using System.Collections.Generic;

namespace Quotes.Models
{
    public class QuoteResponse
    {
        public List<Citacao> quotes;
    }

    public class Citacao
    {
        public int id_citacao;
        public int id_categoria;
        public string citacao;
        public string autor;
        public string autor_detalhes;
        public string tag_pesquisa;
        public string ativo;
        public string dtcadastro;
        public string dtalteracao;
    }
}
