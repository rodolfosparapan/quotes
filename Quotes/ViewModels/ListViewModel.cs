﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Quotes.Models;
using Quotes.Services;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;

namespace Quotes.ViewModels
{
    public class ListViewModel : BaseViewModel
    {
        public ObservableCollection<Quote> Quotes { get; set; }
        private List<Quote> quotesFull;
        public ICommand EditQuoteCommand { get; private set; }
        public ICommand DeleteQuoteCommand { get; private set; }

        private bool loading = false;
        public bool Loading
        {
            get { return loading; } 
            set { loading = value; OnPropertyChanged();}
        }

        private string search;
        public string Search
        {
            get { return search; } 
            set{ search = value;
                this.Quotes.Clear();
                if (!String.IsNullOrEmpty(search))
                {
                    var filteredQuotes = this.quotesFull.Where(q => q.Text.ToLower().Contains(search.ToLower())).ToList();
                    filteredQuotes.ForEach(this.Quotes.Add);
                }
                else{
                    this.quotesFull.ForEach(this.Quotes.Add);
                }
            }
        }

        private IService<Quote> quotesService;

        public ListViewModel()
        {
            this.Quotes = new ObservableCollection<Quote>();

            //this.quotesService = new QuotesHttpService();
            this.quotesService = new QuotesMockData();

            this.BindCommands();
        }

        public async Task GetQuotes()
        {
            this.Quotes.Clear();
            this.Loading = true;
            this.quotesFull = await this.quotesService.ListAsync();
            this.quotesFull.ForEach(this.Quotes.Add);
            this.Loading = false;
        }

        private void BindCommands()
        {
            this.EditQuoteCommand = new Command<Quote>((quote) => {
                MessagingCenter.Send<BaseViewModel, Quote>(this, "PushEditPage", quote);
            });

            this.DeleteQuoteCommand = new Command<Quote>(async (quote) => {
                try
                {
                    if (await this.quotesService.DeleteAsync(quote))
                    {
                        this.quotesFull.Remove(quote);
                        this.Quotes.Remove(quote);
                        MessagingCenter.Send<ListViewModel, string>(this, "DeleteMessage", "Message was delete successfully!");
                    }
                }
                catch
                {
                    MessagingCenter.Send<ListViewModel, string>(this, "DeleteMessage", "Was not possible delete yout Quote");
                }
            });
        }
    }
}
