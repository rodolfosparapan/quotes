﻿using System;
using Xamarin.Forms;
using Quotes.Models;
using System.Windows.Input;
using Quotes.Services;

namespace Quotes.ViewModels
{
    public class AddViewModel : BaseViewModel
    {
        private Quote currentQuote;
        private IService<Quote> quotesService;
        public ICommand SaveCommand { get; }

        public string Text { 
            get{ return currentQuote.Text; } 
            set
            {
                currentQuote.Text = value;
                ((Command)this.SaveCommand).ChangeCanExecute();
            } 
        }

        public string Author { 
            get{ return currentQuote.Author; } 
            set
            {
                currentQuote.Author = value;
                ((Command)this.SaveCommand).ChangeCanExecute();
            } 
        }

        public AddViewModel(Quote quote = null)
        {
            this.currentQuote = (quote != null ? quote : new Quote());
            this.quotesService = new QuotesMockData();

            this.SaveCommand = new Command(async () => {
                try
                {
                    if (this.currentQuote.Id > 0)
                    {
                        await this.quotesService.UpdateAsync(this.currentQuote);
                        MessagingCenter.Send<AddViewModel, string>(this, "AddMessage", "Quote updated!");
                        MessagingCenter.Send<BaseViewModel>(this, "PopEditPage");
                    }
                    else
                    {
                        await this.quotesService.AddAsync(this.currentQuote);
                        MessagingCenter.Send<AddViewModel, string>(this, "AddMessage", "Quote successfully added!");
                        MessagingCenter.Send<BaseViewModel, int>(this, "SetTabbledPageFocus", 0);
                    }
                }
                catch
                {
                    MessagingCenter.Send<AddViewModel,string>(this, "AddMessage", "Error when trying add quote!");
                }

        }, CanSaveQuote);
        }

        private bool CanSaveQuote()
        {
            return !String.IsNullOrEmpty(this.Text) && !String.IsNullOrEmpty(this.Author);
        }
    }
}
