﻿using System;
using Quotes.Views;
using Xamarin.Forms;

namespace Quotes
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new MainPage();
        }
    }
}
